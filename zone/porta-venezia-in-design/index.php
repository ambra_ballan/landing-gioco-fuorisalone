
<?php
	include ("../includes/header.php");
?>


					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/porta-venezia-in-design/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Porta Venezia in Design</h1>
							</div>

	            <img class="half" src="../img/venezia.png" alt="Porta Venezia in Design"/>

	            <p class="half">
								Porta Venezia in Design, il circuito fuorisalone che abbina la contemporaneità del Design e dell’arte all’eredità architettonica e decorativa dello stile Liberty - di cui è particolarmente ricca questa zona della città - per renderla maggiormente nota ai cittadini e agli ospiti stranieri che visitano Milano durante la design week. <br /><br />
								Un circuito ‘a tre’: showroom, negozi e aziende che espongono le novità di design e arte, il focus culturale sul Liberty e il percorso food&amp;wine, che trova spazio in alcuni selezionati locali e ristoranti, con la creazione di menu ideati per il fuorisalone, ma che ospitano anche mostre o contenuti legati al food-design.<br /><br />
								L’idea su cui si basa il circuito è evidenziare i luoghi, talvolta visibili, oppure ‘nascosti’: aziende del settore design/arredo, luoghi di ospitalità con buon cibo e vino, studi di architettura e laboratori. Ma anche l’aspetto culturale, legato all’eredità Liberty in zona, o la possibilità di svelare luoghi inaccessibili per anni, come l’Albergo Diurno Venezia, grazie alla Delegazione FAI di Milano.

								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Ogni giocatore iscritto al FAI inizia la partita con 2 gettoni movimento aggiuntivi</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
