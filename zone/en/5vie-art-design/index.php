<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a href="../../5vie-art-design/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>
          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>5vie art+design</h1>
							</div>

	            <img class="half" src="../../img/5vie.png" alt="Mappa Design District"/>

	            <p class="half">
								5vie, the oldest and most historic district of Milan, is transformed into a new district with a
								high artistic-cultural value projected towards design and innovation.<br /><br />
								Living up to the values of its mission: history, culture and innovation, 5VIE Art + Design has,
								in the past editions of the Milan Design Week, carried out its own territorial and cultural
								marketing project for the enhancement of Milan’s historic city centre.<br /><br />
								Literally the 5VIE district comprises Via Santa Marta, Via Santa Maria Podone, Via Santa
								Maria Fulcorina, Via Bocchetto and Via del Bollo, but in these past years it has widened and
								its now characterized by archaeological finds, beautiful cloisters, treasures of modern
								architecture and new areas to be recovered.
							<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>All players who do not know who Maurizio Cattelan is, when playing they can not move their game piece on “Piazza Affari”</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone.</p>
								<a class="bottone" href="../../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
