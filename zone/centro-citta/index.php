
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/centro-citta/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Centro Città</h1>
							</div>

	            <img class="half" src="../img/centro.png" alt="Mappa Centro Città"/>

	            <p class="half">
								Uno degli scenari più rappresentativi del Fuorisalone è il centro della città di Milano; questa zona è fondamentalmente caratterizzata da eventi ubicati all'interno degli Showroom delle storiche firme del design made in italy, soprattutto via Durini è sempre stata una meta fondamentale di ogni edizione del fuorisalone, al quale si sommano il quadrilatero della moda e via Manzoni.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Ogni giocatore che ha visto almeno una volta in vita sua il Duomo di Milano con la nebbia, inizia la partita con due punti vittoria</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
