
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a href="../../porta-venezia-in-design/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Porta Venezia in Design</h1>
							</div>

	            <img class="half" src="../../img/venezia.png" alt="Porta Venezia in Design"/>

	            <p class="half">
								Porta Venezia in Design, is the Fuorisalone circuit that combines the contemporaneity of art
and design to the architectural and decorative heritage of the Art Nouveau style (aka Liberty
in italy) - of which this area of ​​the city is particularly rich - to make it more known to Milanese
citizens and to foreigners who visit Milan for the design week.<br /><br />
A 3- way circuit: showrooms, shops and companies displaying the news in design and art,
the cultural focus is on Art Nouveau style and the food &amp; wine route, which finds its way
through selected restaurants and bars, which serve exceptional menus specifically designed
for the fuorisalone, but which also host exhibitions or events related to food-design.<br /><br />
The idea of the circuit is studied to highlight places, which are at times visible, or may be
hidden: companies belonging to the design/furniture industries, places of hospitality which
offer good food and wine, architectural studios and workshops. But also the cultural aspect,
linked to the Art Nouveau style legacy in the area, or the possibility of unveiling places that
have been inaccessible for years, like the Albergo Diurno Venezia, thanks to the Milan FAI
Delegation.

								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>Each player registered with FAI starts the game with 2 additional movement coins</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
