<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/brera-design-district/">en</a></div>
					</div>
          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Brera Design District</h1>
							</div>

	            <img class="half" src="../img/brera.png" alt="Mappa Design District"/>

	            <p class="half">
							Brera Design District è un progetto che vede protagonista lo storico quartiere di Milano, famoso per il suo importante patrimonio di arte e storia e per il contributo che da secoli fornisce alla vita culturale della città.<br /><br />
							Brera Design District è la sede di oltre 100 showroom di design che ne fanno un punto di riferimento per il settore e da sempre coniugano l’eredità artistica e artigianale del quartiere con una forte spinta alla sperimentazione e all’innovazione.<br /><br />
							<a class="brera" href="http://www.breradesigndistrict.it/?lang=en" target="_blank">www.breradesigndistrict.it</a>
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Tutti i giocatori che posseggono realmente uno o più icone del design inserite nelle carte obiettivo del gioco, iniziano la partita con un punto vittoria</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
