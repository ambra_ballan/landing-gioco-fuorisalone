
<?php
	include ("../includes/header.php");
?>
					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/ventura-future/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Ventura Lambrate</h1>
							</div>

	            <img class="half" src="../img/ventura.png" alt="Mappa Ventura Lambrate"/>

	            <p class="half">
								Nel 2010 nasceva Ventura Lambrate, punto di riferimento per giovani creativi e appassionati di design, una tappa fissa tra i tanti appuntamenti che popolano la Milano Design Week.
								Definita come la nuova Brooklyn italiana, Lambrate ci isola dal caos cittadino, per trasportarci in un quartiere che profuma di idee e novità tra le tre strade principali di via Massimiano, via Ventura e via Conte Rosso. <br /><br />
								Nel 2017 ha aperto i battenti Ventura Centrale ai magazzini raccordati di Stazione Centrale e l’edizione 2018 della Milano Design Week darà il benvenuto a Ventura Future, una nuova destinazione da segnare in agenda che troverà spazio presso Future Dome, palazzo storico in stile Liberty in via Paisiello, a due passi dalla fermata di Piola.

								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Tutti i giocatori che hanno frequentato una scuola di design iniziano la partita con 2 gettoni movimento aggiuntivi
</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
