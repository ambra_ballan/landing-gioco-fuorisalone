
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/tortona-design-week/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Tortona Design Week</h1>
							</div>

	            <img class="half" src="../img/tortona.png" alt="Mappa Tortona"/>

	            <p class="half">
								Nel quartiere di Porta Genova ha preso forma, alle spalle della stazione dei treni, su poche vie che portano verso l'esterno della città, una serie di eventi che hanno creato una delle zone più giovani e alternative del fuorisalone.<br /><br />
								Tortona Design Week è un progetto di comunicazione integrata con il patrocinio del Comune di Milano, con l’obiettivo di valorizzare eventi, installazioni e progetti che si svolgono nell’area di Tortona durante il fuorisalone.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Ogni giocatore prima di iniziare la partita deve condividere sui social una foto con #fuorisalonegioco, il giocatore che al termine della partita avrà guadagnato più “like” riceverà un bonus di 3 punti vittoria.</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
