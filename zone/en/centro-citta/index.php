
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a href="../../centro-citta/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Centro Città</h1>
							</div>

	            <img class="half" src="../../img/centro.png" alt="Mappa Centro Città"/>

	            <p class="half">
								One of the most representative scenarios of fuorisalone is Milan’s city center; this area is
								fundamentally characterized by events located in the Showrooms of historic made in Italy
								design brands. Via Durini especially, has always been a fundamental stop in every edition of
								the fuorisalone, in addition to the Quadrilatero della moda (fashion square) and via manzoni.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>Every player who has seen the Duomo di Milano (Milan’s Cathedral) at least once in his/her
life with fog, begins the game with 2 victory points</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone.</p>
								<a class="bottone" href="../../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
