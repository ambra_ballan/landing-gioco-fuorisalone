
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a href="../../tortona-design-week/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Tortona Design Week</h1>
							</div>

	            <img class="half" src="../../img/tortona.png" alt="Mappa Tortona"/>

	            <p class="half">
								In the Porta Genova district, behind the train station, on a few streets leading to the outside
								of the city, a series of events have initiated one of the youngest and unconventional areas of
								the fuorisalone.<br /><br />
								Tortona Design Week is an integrated communication project under the patronage of the
								Municipality of Milan, with the aim of enhancing events, installations and projects taking
								place in the area of Tortona during the fuorisalone.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>Before starting the game, each player must share a photo with #fuorisalonegioco on the
							social networks, the player who at the end of the game will have earned more “like” will
							receive 3 victory points as a bonus</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
