
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/extra-zone/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Extra Zone</h1>
							</div>

	            <img class="half" src="../img/extra.png" alt="Extra zone"/>

	            <div class="half">
								<ul class="slider">
									<li><p>
										<strong>La Triennale di Milano</strong><br /><br />

										Seppur spostata rispetto ai punti nevralgici della settimana del design, la Triennale di Milano è da anni una location fondamentale. Eventi, mostre e performance vengono organizzate sia all'interno del palazzo dell'arte, sia nei giardini esterni all'edificio, con installazioni e presentazioni inerenti al design, all'arredamento ma anche ai nuovi stili di vita.
									</p></li>
									<li><p>
										<strong>Fabbrica del Vapore</strong><br /><br />

										Uno dei luoghi di maggiore interesse della settimana del design milanese sono gli spazi espositivi ubicati nell'area delle ex sede Enel riconvertita a cittadella creativa multidisciplinare. La Fabbrica del vapore è attiva dal 2004 e sin dal suo debutto il design viene affrontato con un occhio di riguardo nei confronti delle tematiche sociali, economiche, politiche ed ecologiche.
									</li>
									<li><p>
										<strong>Bar Basso</strong><br /><br />

										Ormai da decenni, nelle notti della settimana del design, è necessario frequentare il Bar Basso per incontrare i designer e il popolo del fuorisalone. Serate a base di Negroni Sbagliato e Mangia e Bevi sono un must, la designer francese Ionna Vautrin ha confessato in un’intervista: “What happens at Bar Basso remains at Bar Basso”.
									</p></li>
									<li><p>
										<strong>Piazza Gae Aulenti</strong><br /><br />

										Se Piazza Duomo è il centro storico di Milano, Piazza Gae Aulenti ne rappresenta il volto nuovo, simbolo della città che cambia e guarda al futuro.
										Progettata dall'architetto argentino Cesar Pelli, a completamento delle omonime torri, è stata inaugurata l’8 dicembre 2012 e dedicata all'architetto e designer Gae Aulenti. Una dedica significativa a una donna che, grazie al suo talento, ha saputo distinguersi in tempi e contesti prettamente maschili.
									</p></li>
									<li><p>
										<strong>Università Statale</strong><br /><br />

										L'Università degli Studi di Milano è la più grande istituzione universitaria milanese e della Lombardia.
										Durante il fuorisalone nella sede centrale in via Festa del Perdono, la rivista Interni organizza da anni moltissime installazioni in collaborazione con importanti brand e istituzioni.
									</p></li>
									<li><p>
										<strong>Fondazione Prada</strong><br /><br />

										La Fondazione Prada è un'istituzione dedicata all'arte contemporanea e alla cultura.
										All’interno della nuova sede permanente a Milano, la Fondazione si interroga su quali siano gli intenti e la rilevanza dell’impegno culturale oggi, con una serie di progetti in continua evoluzione.
									</p></li>
							  </ul>
	            </div>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Tutti i giocatori che non hanno mai visitato la Triennale di Milano, nel gioco non possono spostare la propria pedina su di essa</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>


<?php
	include ("../includes/footer.php");
?>
