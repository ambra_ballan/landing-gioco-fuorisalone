
<?php
	include ("../includes/header.php");
?>
					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/zona-santambrogio/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Zona Santambrogio</h1>
							</div>

	            <img class="half" src="../img/sambrogio.png" alt="Mappa Sant'Ambrogio"/>

	            <p class="half">
								Zona Santambrogio Milan Design Week è una delle zone della città più rilevante dal punto di vista storico, artistico e culturale: Piazza Castello, la Basilica di Sant’Ambrogio, la Fondazione Achille Castiglioni, la Fondazione Franco Albini e il concept store Rossana Orlandi sono solo alcune delle eccellenze presenti nella zona.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Tutti i giocatori che si chiamano Ambrogio/a, Achille, Franco/a e Rossana, iniziano la partita con 2 gettoni movimento aggiuntivi</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
