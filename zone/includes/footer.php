<footer>
   <div class="container">
     <div class="fs">
       <a href="http://fuorisalone.it"><img src="../img/fuorisalone-logo-footer.png" alt="Fuorisalone.it"></a>
     <br><br><br>
     </div>
     <div>
       <div class="corporate">
         Fuorisalone.it&reg; <br> è un progetto di Studiolabo S.r.l<br>
         Via Palermo 1<br>
         20121 - Milano
         <br><br>
         T.  <a href="tel:00390236638150">+39 02 36638150</a><br>
         F. +39 02 36638150<br>
         <a href="mailto:info@studiolabo.it">info@studiolabo.it</a><br>
         <a href="http://www.studiolabo.it">www.studiolabo.it</a>
       </div>
       <div class="contacts">
         UFFICIO STAMPA<br>
         Communication&amp;PR<br>
         Martina Gamboni - Strategic Footprints<br>
         <a href="mailto:press@martinagamboni.it">press@martinagamboni.it</a>
         <br><br>
         MEDIA KIT<br>
         <a href="CS+Media_Kit_gioco.zip">Download</a>
       </div>
       <div class="disclaimer">
         <p>© 2003-2018 FUORISALONE.IT® <br>Marchio registrato da Studiolabo S.r.l. <br>È severamente vietata ogni riproduzione non autorizzata del marchio e dei contenuti di questo sito.	<br>					<br>
           <a href="http://fuorisalonemagazine.it/it/privacy-policy">Privacy Policy</a> - <a href="http://fuorisalonemagazine.it/it/policies/cookies-policy">Cookies Policy</a>
         </p>
       </div>
     </div>
   </div>
</footer>




 <!-- per effetto parallax-->
 <script>
 function firePixel() {
   fbq('track', 'InitiateCheckout');
 }
 </script>

 <!-- per effetto parallax-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>

 <script type="text/javascript">
 function calcolaltezze(elemento, variabile)
 {
     // rendo uguale l'altezza di tutti gli elementi
     elemento.height("auto");
     var variabile = 0;
     elemento.each(function () {
         if ($(this).height() > variabile) {
             variabile = $(this).height();
         }
     });
     elemento.height(variabile);
 }


 var larghezzafinestra = $(window).outerWidth();

 $( document ).ready(function() {

   if (larghezzafinestra > 1024) {
     var scene = $('#scena').get(0);
     var parallax = new Parallax(scene);
   } else {

   };

   var slider = $('.bxslider').bxSlider({
   mode: 'fade',
   auto: true,
   pause: 4000,
   responsive: true,
   adaptiveHeight: false
   });

   if(larghezzafinestra < 1024) {
     minSlides = 1;
     slideWidth = 8000
   } else {
     minSlides = 3;
     slideWidth = 400;
   };

   var slider = $('.carousel').bxSlider({
   minSlides: minSlides,
   maxSlides: '3',
   pause: 4000,
   slideWidth:slideWidth
   });

   calcolaltezze($('.evento strong'), "altezza");
   calcolaltezze($('.evento'), "altezza");
   calcolaltezze($('.dove'), "altezza");
 });

 $(window).scroll(function() {
   var sposta = ( jQuery(window).scrollTop() *0.4);
   //alert(sposta);
   var scroll = $(window).scrollTop();
   if (scroll >= 500 )  {
     $(".flash").css("display", "block");
   } else {
     $(".press").css("top", sposta);
     $(".flash").css("display", "none");
   }
 });

</script>

</body>
</html>
