<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/porta-romana/">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>Porta Romana</h1>
							</div>

	            <img class="half" src="../img/romana.png" alt="Mappa Porta Romana"/>

	            <p class="half">
								Il quartiere di Porta Romana era considerato l’ingresso monumentale di Milano per secoli, grazie alla presenza dell’arco monumentale voluto da Filippo III di Spagna nel 1596. Secondo una leggenda cittadina il diavolo viveva in queste vie, ma in realtà si trattava di un marchese stravagante, Ludovico Acerbi.<br /><br />
								Oggi le vie della zona non sono infestate da presenze mefistofeliche, ma sono popolate da creativi-ecosostenibili dediti alle buone pratiche.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Chi tra i giocatori ha studiato allo IED inizia la partita con un punto vittoria </p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Condividi con noi la tua partita</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
