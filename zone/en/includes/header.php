
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Fuorisalone.it | The board game</title>

        <meta name="robots" content="noindex">

        <meta name="viewport" content="width=device-width, initial-scale=1">


        <!-- Place favicon.ico in the root directory -->
        <link rel="icon"  type=”image/ico” href="../../img/favicon.ico" />
  	    <link rel="shortcut icon" type="image/x-icon" href="../../img/favicon.ico" />
        <link rel="apple-touch-icon" href="../../img/apple-touch-icon.png">

        <!-- google font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <!-- jQuery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- bxSlider Javascript file-->
        <script src="../../assets/jquery.bxslider.min.js"></script>
        <!-- bxSlider CSS file -->
        <link href="../../assets/jquery.bxslider.css" rel="stylesheet" />

        <link href="../../assets/main.css" rel="stylesheet" />


        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1745335929113048');
        fbq('track', 'PageView');
        </script>
        <noscript>
         <img height="1" width="1"
        src="https://www.facebook.com/tr?id=1745335929113048&ev=PageView
        &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Analytics -->
        <script type="text/javascript">
      		/* <![CDATA[ */
      		(function (i, s, o, g, r, a, m) {
      			i['GoogleAnalyticsObject'] = r;
      			i[r] = i[r] || function () {
      				(i[r].q = i[r].q || []).push(arguments);
      			}, i[r].l = 1 * new Date();
      			a = s.createElement(o),
      					m = s.getElementsByTagName(o)[0];
      			a.async = 1;
      			a.src = g;
      			m.parentNode.insertBefore(a, m);
      		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

      		ga('create', 'UA-210523-1', 'auto');
      		ga('send', 'pageview');

      		/* ]]> */
      	</script>
        <!-- end Analytics -->

        <script type="text/javascript">

				$( document ).ready(function() {

					var slider = $('.slider').bxSlider({
					auto: true,
					minSlides: 1,
					maxSlides: 1,
					pause: 4000
					});

				});

				</script>





    </head>

    <body>

        <header>
          <div class="top">
            <img src="../../img/logo.png" alt="logo fuorisalone board game"/>
            <a class="trans bottone" href="http://www.craniocreations.it/prodotto/fuorisalone/?utm_source=fs_gioco" target="_blank" onclick="return firePixel();">Buy now</a>
            <a href="http://www.craniocreations.it/prodotto/fuorisalone/?utm_source=fs_gioco" target="_blank" onclick="return firePixel();" class="bottone mobile"></a>
