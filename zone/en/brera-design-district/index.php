<?php
	include ("../includes/header.php");
?>


					<div class="lingue"><a href="../../brera-design-district/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>
          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Brera Design District</h1>
							</div>

	            <img class="half" src="../../img/brera.png" alt="Mappa Design District"/>

	            <p class="half">
								Brera Design District is a project that centres on the Milanese historic district, famous for its
								important artistic and historical heritage and for the influence and impact that for centuries it
								has had on the cultural life of the city.<br /><br />
								Brera Design District is home to over 100 design showrooms that make it a point of
								reference for the design industry, supporting to combine the artistic and artisanal heritage of
								the area with a strong drive towards experimentation and innovation.<br /><br />
							<a class="brera" href="http://www.breradesigndistrict.it" target="_blank">www.breradesigndistrict.it</a>
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>All players who actually own one or more design icons included in the game&#39;s objective
cards will start the game with a victory point</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print
										and use when you play Fuorisalone.</p>
								<a class="bottone" href="../../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
