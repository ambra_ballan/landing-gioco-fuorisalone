<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a href="../../porta-romana/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Porta Romana</h1>
							</div>

	            <img class="half" src="../../img/romana.png" alt="Mappa Porta Romana"/>

	            <p class="half">
								The Porta Romana district was considered for centuries the monumental entrance to the city
								of Milan, due to the monumental arch built by Philip III of Spain in 1596. According to a city
								legend, the devil lived in these streets, but in reality it was an extravagant marquis, known as
								Ludovico Acerbi.<br /><br />
								Today the streets of the area are not haunted by Mephistophelic presences, but are
								populated by eco-sustainable creative professionals dedicated to good practices.
								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>The players who have studied at the IED starts the game with 1 victory point</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
