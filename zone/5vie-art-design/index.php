<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a class="active">it</a>&nbsp;/&nbsp;<a href="../en/5vie-art-design/">en</a></div>
					</div>
          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../img/scimmia.png" alt="Illustrazione scimmietta"/>
								<h1>5vie art+design</h1>
							</div>

	            <img class="half" src="../img/5vie.png" alt="Mappa Design District"/>

	            <p class="half">
								5vie il quartiere più antico e più ricco di storia di Milano si trasforma in un nuovo distretto dall’altissimo valore artistico-culturale proiettato verso il design e l’innovazione.<br /><br />
								Tenendo fede ai valori della propria mission: storia, cultura e innovazione, 5VIE Art+Design ha, nelle passate edizioni delle Milano Design Week, portato avanti il proprio progetto di marketing territoriale e culturale per la valorizzazione del centro storico di Milano.<br /><br />
								Se letteralmente le 5VIE sarebbero Via Santa Marta, Via Santa Maria Podone, Via Santa Maria Fulcorina, Via Bocchetto e Via del Bollo, sicuramente in questi anni è stato creato un distretto ampio caratterizzato da reperti archeologici, bellissimi chiostri, perle di architettura moderna e nuovi spazi in via di recupero.

							<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../img/regola.jpg" alt="REGOLA FACOLTATIVA"/><br />
							<p>Tutti i giocatori che non sanno chi è Maurizio Cattelan, giocando non possono spostare la propria pedina su “Piazza Affari”.</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../img/tantopiove.png" alt="espansione tanto piove"/><br />
								<p><strong>Scarica l’espansione “Tanto piove!”</strong> con tante carte imprevisto da stampare e usare nelle tue prossime partite a Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ITA.pdf" target="_blank">Scarica l'espansione</a>
							</div>
							<img class="half" src="../img/piove.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
