
<?php
	include ("../includes/header.php");
?>

					<div class="lingue"><a href="../../ventura-future/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
					</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Ventura Lambrate</h1>
							</div>

	            <img class="half" src="../../img/ventura.png" alt="Mappa Ventura Lambrate"/>

	            <p class="half">
								In 2010 Ventura Lambrate was created, a reference point for young creative professionals
and design lovers, a fixed stop among the many events that populate the Milan Design
Week.<br /><br />
Defined as the new Italian Brooklyn, Lambrate isolates its visitors from the chaos of the city,
to transport them in a neighbourhood that pours of ideas and novelties throughout the three
main streets of via Massimiano, via Ventura and via Conte Rosso. <br /><br />
In 2017 Ventura Centrale
opened in the warehouses connected to Stazione Centrale and the 2018 edition of the Milan
Design Week will welcome Ventura Future, a new destination to be marked on the agenda

that will rise at Future Dome, a historic Art Nouveau building in via Paisiello, a few steps
from Piola Metro stop.

								<br /><br />
	            </p>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>All players who have attended a design school will start the game with 2 additional
movement coins</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone.</p>
								<a class="bottone" href="../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>

<?php
	include ("../includes/footer.php");
?>
