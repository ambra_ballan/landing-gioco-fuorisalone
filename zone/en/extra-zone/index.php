
<?php
	include ("../includes/header.php");
?>

				<div class="lingue"><a href="../../extra-zone/">it</a>&nbsp;/&nbsp;<a class="active">en</a></div>
				</div>

          <div class="info">
						<div class="container">
							<div class="titolo">
								<img src="../../img/scimmia.png" alt="illustration scimmietta"/>
								<h1>Extra Zone</h1>
							</div>

	            <img class="half" src="../../img/extra.png" alt="Extra zone"/>

	            <div class="half">
								<ul class="slider">
									<li><p>
										<strong>La Triennale di Milano</strong><br />

										Although shifted compared to the focal points of the design week, La Triennale di Milano
has been a key location for decades. Events, exhibitions and performances are organized
both inside the art building and in the gardens outside the building, with installations and
presentations related to design, furnishings and new lifestyles.
									</p></li>
									<li><p>
										<strong>Fabbrica del Vapore</strong><br /><br />

										One of the places of great interest in the Milan design week are the exhibition spaces
located in the area of the former Enel headquarters converted into a multidisciplinary
creative citadel. The Fabbrica del Vapore has been active since 2004 and since its debut
design has been approached there with a special focus on social, economic, political and
ecological issues.
									</li>
									<li><p>
										<strong>Bar Basso</strong><br /><br />

										For decades now, during the nights of the design week, it is necessary to attend the Basso
Bar to meet the fuorisalone designers and people. Evenings accompanied by Negroni
Sbagliato and Mangia e Bevi are a must, the French designer Ionna Vautrin confessed in an
interview: &quot;What happens at Bar Basso remains at Bar Basso&quot;.
									</p></li>
									<li><p>
										<strong>Piazza Gae Aulenti</strong><br /><br />

										If Piazza Duomo is the historic centre of Milan, Piazza Gae Aulenti represents its new face, symbol of the city that changes and looks to the future.
Designed by the Argentine architect Cesar Pelli, to complete the homonymous towers, it was
inaugurated on December 8, 2012 and dedicated to the architect and designer Gae Aulenti.
A significant dedication to a woman who, thanks to her talent, has been able to stand out in
masculine times and contexts.
									</p></li>
									<li><p>
										<strong>Università Statale</strong><br /><br />

										Università degli Studi di Milano is the largest university institution in Milan and and in
Lombardy. During the fuorisalone in the University’s headquarters in via Festa del Perdono, Interni
magazine has been organizing many installations in collaboration with major brands and
institutions.
									</p></li>
									<li><p>
										<strong>Fondazione Prada</strong><br /><br />

										Fondazione Prada is an institution dedicated to contemporary art and culture.
Within the new permanent headquarters in Milan, the Fondazione questions nowadays
commitment and engagement towards culture, with a series of constantly evolving projects.
									</p></li>
							  </ul>
	            </div>
          	</div>
        	</div>
        </header>
        <main>
          <div class="fascia regola">
						<div class="container">
						<img src="../../img/rule.jpg" alt="OPTIONAL RULE"/><br />
							<p>All players, who have never visited La Triennale di Milano, can not move their game piece
on it during the game.</p>
						</div>
				  </div>
					<div class="fascia espansione">
						<div class="container">
							<div class="half">
								<img src="../../img/raining.png" alt="expansion it's raining"/><br />
								<p><strong>Download the expansion “It’s Raining!”</strong> with many community chest cards to print and use when you play Fuorisalone</p>
								<a class="bottone" href="../../estensioni/Carte_imprevisto_da stampare_ENG.pdf" target="_blank">Download the expansion</a>
							</div>
							<img class="half" src="../../img/rain.png" alt="Carte Tantopiove"/>
						</div>
					</div>
					<div class="fascia social">
						<div class="container">
							<div class="ash">
								<p><strong>Share your game with us</strong></p>
								<img src="../../img/ashtag.png" alt="#FUORISALONEGIOCO"/><br />
							</div>
							<div class="links">
								<p>Follow us</p>
								<a href="https://it-it.facebook.com/fuorisalone.it/" target="_blank"><img src="../../img/fb.svg" alt="facebook fuorisalone"/></a>
								<a href="https://www.instagram.com/fuorisalone/" target="_blank"><img src="../../img/instagram.svg" alt="icona instagram fuorisalone"/></a>
							</div>
						</div>
					</div>
			  </main>


<?php
	include ("../includes/footer.php");
?>
